﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.FileDatabase;

namespace TaxiService.Store
{
    public class MemoryStore<T> : FileStore<T> where T : BaseEntity
    {
        public MemoryStore(IEqualityComparer<T> comparer) : base(comparer: comparer, serializer: null, fileName: null)
        {
        }


        public override void Flush()
        {
            throw new NotSupportedException("You cannot flush a Memory Store");
        }
    }
}
