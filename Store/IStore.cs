﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.FileDatabase;

namespace TaxiService.Store
{
    public interface IStore<T> where T: BaseEntity
    {
        IEnumerable<T> GetAll();
        void Flush();
        void Add(T data);
        void Remove(T data);
        void Update(T data);
        bool Exists(T data);
        IEnumerable<T> Filter(Func<T, bool> condition);
        T FindById(int id);
    }
}
