﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.FileDatabase;

namespace TaxiService.Store
{
    public class FileStore<T>: IStore<T> where T: BaseEntity
    {
        private IList<T> entities;
        private ISerializer<T> _serializer;
        private string _fileName;
        private IEqualityComparer<T> _equalityComparer;
        public FileStore(ISerializer<T> serializer, IEqualityComparer<T> comparer, string fileName)
        {
            _serializer = serializer;
            _fileName = fileName;
            entities = OpenAndReturnIEnumerable(fileName).ToList<T>();
            _equalityComparer = comparer;
        }

        public void Add(T data)
        {
            entities.Add(data);
        }

        public T FindById(int id)
        {
            return entities.First(_ => _.Id == id);
        }

        public virtual void Flush()
        {
            SaveToFileIEnumerable(GetAll(), _fileName);
        }

        public IEnumerable<T> GetAll() => entities.Select(_ => _).Distinct(_equalityComparer);

        public void Remove(T data)
        {
            entities.Remove(data);
        }

        public void Update(T data)
        {
            if (!Exists(data))
            {
                Add(data);
                return;
            }
            entities[entities.IndexOf(data)] = data;
        }

        private IEnumerable<T> OpenAndReturnIEnumerable(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return Enumerable.Empty<T>();
            }
            fileName = NormalizeFileName(fileName, _serializer.GetExtension());
            if (!File.Exists(fileName))
            {
                return Enumerable.Empty<T>();
            }
            string allRawData = File.ReadAllText(fileName);
            string[] dataSplittedByDelimiter =
                allRawData.Split(new[] { BinaryDelimiterForSerialization }, StringSplitOptions.RemoveEmptyEntries);
            IList<T> data = new List<T>();
            foreach (var stringEntity in dataSplittedByDelimiter)
            {
                data.Add(_serializer.Deserialize(stringEntity));
            }
            return data;
        }

        private void SaveToFileIEnumerable(IEnumerable<T> obj, string fileName)
        {
            fileName = NormalizeFileName(fileName, _serializer.GetExtension());
            using (var sw = new StreamWriter(fileName))
            {
                foreach (var singleEntity in obj)
                {
                    string singleEntitySerialized = _serializer.Serialize(singleEntity);
                    sw.Write(singleEntitySerialized);
                    sw.Write(BinaryDelimiterForSerialization);
                }
            }
        }
        private static string BinaryDelimiterForSerialization = "\x10\n";
        private static string NormalizeFileName(string fileName, string expectedExtension)
        {
            fileName = Path.GetFileNameWithoutExtension(fileName);
            fileName = string.Format("{0}.{1}", fileName, expectedExtension);
            return fileName;
        }

        public bool Exists(T data)
        {
            return entities.IndexOf(data) != -1;
        }

        public IEnumerable<T> Filter(Func<T, bool> condition)
        {
            return GetAll().Where(condition);
        }
    }
}
