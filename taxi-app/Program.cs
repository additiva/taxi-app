﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows.Forms;
using System.Threading;
using TaxiService;
using TaxiService.Repository;
using TaxiService.Event;
using Infrastructure;

namespace TaxiService
{
    class Program
    {
        private static CarRepository carRepo;
        private static HumanRepository humanRepo;
        private static OrderRepository orderRepo;
        private static ReviewRepository reviewRepo;
        static Program()
        {
            InjectedServiceLocator.RegisterAll();
            EventIntializer.InitDelegates();

            carRepo = InjectedServiceLocator.Get<CarRepository>();
            humanRepo = InjectedServiceLocator.Get<HumanRepository>();
            orderRepo = InjectedServiceLocator.Get<OrderRepository>();
            reviewRepo = InjectedServiceLocator.Get<ReviewRepository>();

        }
        static void Main(string[] args)
        {

            #region Human Repository adding info
            humanRepo.Persist(new Passenger("Vasile Jereghi", new DateTime(1974, 12, 4), "+373 60 456 604", true));
            humanRepo.Persist(new Passenger("Eranche Ion", new DateTime(1934, 2, 12), "+373 60 345 804", true));
            humanRepo.Persist(new Passenger("Alexandru Smoltnik", new DateTime(1954, 11, 11), "+373 60 785 443", true));
            humanRepo.Persist(new Passenger("Denis Oleac", new DateTime(1895, 12, 7), "+373 60 456 563", true));

            humanRepo.Persist(new Operator("Maria Ivanovna", new DateTime(1995, 12, 7), "+373 60 555 222", true));
            humanRepo.Persist(new Operator("Victoria Ielanem", new DateTime(1995, 12, 7), "+373 60 555 222", true));

            humanRepo.Persist(new Driver("Jora Driver", new DateTime(1995, 12, 7), "+373 60 555 222", registeredDate: new DateTime(2010, 4, 5)));

            humanRepo.Flush();
            #endregion

            var passengers = humanRepo.Get<Passenger>();
            var operators = humanRepo.Get<Operator>();
            var drivers = humanRepo.Get<Driver>();
            var cars = carRepo.GetEntities();

            //fire up onNumberChanged
            passengers.First().PhoneNumber = "+23";

            #region Car Repository adding info
            var car = new SedanCar(4, "K-CL 400")
            {
                Options = CarOptions.ChildSeat | CarOptions.Music,
                Driver = drivers.First(),
                CarType = new ElectricCar(pricePerKillowatt: 1.33, killowattPerKm: 0.04),
            };

            carRepo.Persist(car);
            carRepo.Persist(new SedanCar(numberOfSeats: 6));
            carRepo.Persist(new TruckCar(trunkVolume: 40.2d)
            {
                CarType = new GasolineCar(litresPerKm: 40, pricePerLitre: 3.4)
            });

            carRepo.Flush();
            #endregion

            #region Order Repository adding info
            orderRepo.Persist(new Order(passengers.First(), new DateTime(2016, 10, 1), Chisinau(), Bucuresti(), cars.First(), true)
            {
                Operator = operators.First(),                
            });
            orderRepo.Persist(new Order(passengers.First(), new DateTime(2016, 10, 1), Bucuresti(), Chisinau(), cars.Last(), true));

            orderRepo.Flush();
            #endregion

            var firstOrder = orderRepo.GetEntities().First();
            //we fire up event, defined in OrderManager
            firstOrder.Arrived();


            //so, if order is done (ex. Arrived called)
            //we can generate invoice


            Invoice<FileType.FileTypePDF>.SaveAndOpen(firstOrder, "me");
            Invoice<FileType.FileTypeTXT>.SaveAndOpen(firstOrder, "me");
            
        }

        static System.Device.Location.GeoCoordinate Chisinau() => new System.Device.Location.GeoCoordinate(47.003670, 28.907089);
        static System.Device.Location.GeoCoordinate Bucuresti() => new System.Device.Location.GeoCoordinate(44.42683, 26.10254);
    }
}
