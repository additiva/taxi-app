﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService;
using TaxiService.Event;

namespace Tests
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class EventsTestFixture
    {
        private static bool firedOrderCompleted = false;
        private static bool firedPhoneChanged = false;
        static EventsTestFixture()
        {
            Subscribe();
        }

        private static void Subscribe()
        {
            Order.OrderManager.orderCompleted += new EventHandler<TaxiService.Event.OrderCompletedArgs>(orderArrivedHandler);
            Passenger.HumanManager.phoneNumberChanged += new EventHandler<PhoneNumberChangedArgs>(phoneChangedHandler);
        }

        private static void UnSubscribe()
        {
            Order.OrderManager.orderCompleted -= new EventHandler<TaxiService.Event.OrderCompletedArgs>(orderArrivedHandler);
            Passenger.HumanManager.phoneNumberChanged -= new EventHandler<PhoneNumberChangedArgs>(phoneChangedHandler);
        }


        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void OrderShouldCallEventOnArrived()
        {
            var o = new Order(new Passenger(), DateTime.Now, new System.Device.Location.GeoCoordinate(), new System.Device.Location.GeoCoordinate(), new SedanCar());
            o.Arrived();
            Assert.IsTrue(firedOrderCompleted);
        }
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void PassangerShouldCallPhoneChanged()
        {
            var o = new Passenger();
            o.PhoneNumber = "+373 60 31 49 58";
            o.PhoneNumber = "+373 75 31 49 51";
            Assert.IsTrue(firedPhoneChanged);
        }


        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void OrderShouldNotCallEventOnArrived()
        {
            firedOrderCompleted = false;
            UnSubscribe();
            var o = new Order(new Passenger(), DateTime.Now, new System.Device.Location.GeoCoordinate(), new System.Device.Location.GeoCoordinate(), new SedanCar());
            o.Arrived();
            Assert.IsFalse(firedOrderCompleted);
        }
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void PassangerShouldNotCallPhoneChanged()
        {
            firedPhoneChanged = false;
            UnSubscribe();
            var o = new Passenger();
            o.PhoneNumber = "+373 60 31 49 58";
            o.PhoneNumber = "+373 75 31 49 51";
            Assert.IsFalse(firedPhoneChanged);
        }
        private static void orderArrivedHandler(object sender, TaxiService.Event.OrderCompletedArgs r)
        {
            firedOrderCompleted = true;
        }

        private static void phoneChangedHandler(object sender, PhoneNumberChangedArgs e)
        {
            firedPhoneChanged = true;
        }
    }
}
