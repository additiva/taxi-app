﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.Store;

namespace Tests
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class MemoryStoreTestFixture
    {
        private static MemoryStore<BaseEntity> _store;
        static MemoryStoreTestFixture()
        {
            _store = new MemoryStore<BaseEntity>(new BaseEntityEqualityComparer());
        }
        [TestMethod]
        public void StoreShoudBeEmpty()
        {
            Assert.AreEqual(_store.GetAll().Count(), 0);
        }
        [TestMethod]
        [ExpectedException(typeof(NotSupportedException),
    "A MemoryStore should throw NotSupportException on Flush() called")]
        public void StoreShouldThrowNotSupportedException()
        {
            _store.Flush();
        }
    }
}
