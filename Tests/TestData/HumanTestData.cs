﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService;
using TaxiService.Entity;
using TaxiService.Store;

namespace Tests.TestData
{
    internal static class HumanTestData
    {
        internal static IStore<Human> GetHumanStoreWithTestData
        {
            get
            {
                var pasager = new Passenger("Vasile Pasager", new DateTime(1980, 12, 21), "+373 060 389 495");
                var driver = new Driver("Ion Driver", new DateTime(1960, 12, 12), "+373 50 607 659", new DateTime(2010, 1, 23), isMale: true);
                var @operator = new Operator("Operatori Maria", new DateTime(2010, 12, 23), "+373 50 606 894");

                var mazdaCar = new SedanCar(numberOfSeats: 4, vin: "K OE 289");
                mazdaCar.Driver = driver;

                var order = new Order(pasager, DateTime.Now.Subtract(TimeSpan.FromDays(3)), new GeoCoordinate(), new GeoCoordinate(), mazdaCar, urbanStyle: false);
                order.Operator = @operator;
                order.Arrived();

                //todo: make with service locator
                //var memoryStore = InjectedServiceLocator.Get<MemoryStore<Human>>();
                var memoryStore = new MemoryStore<Human>(new BaseEntityEqualityComparer());
                memoryStore.Add(pasager);
                memoryStore.Add(driver);
                memoryStore.Add(@operator);

                return memoryStore;
            }
        }
    }
}
