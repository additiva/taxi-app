﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Store;
using Tests.TestData;

namespace TaxiService.Tests
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class HumanTestFixture
    {
        private static IStore<Human> _store;
        static HumanTestFixture()
        {
            _store = HumanTestData.GetHumanStoreWithTestData;
        }
        [TestMethod]
        public void PassangerShouldHaveNumbersOfOrdersNonZero()
        {
            var pasager = _store.GetAll().OfType<Passenger>().First();
            Assert.IsTrue(pasager.NumberOfOrders > 0);
        }
        [TestMethod]
        public void OperatorShouldHaveProcessedNonZero()
        {
            var op = _store.GetAll().OfType<Operator>().First();
            Assert.IsTrue(op.OrdersProcessed > 0);
        }
        [TestMethod]
        public void DriverShouldHaveSallaryBonusNonZero()
        {
            var driver = _store.GetAll().OfType<Driver>().First();
            Assert.IsTrue(driver.SallaryBonus > 0);
        }
    }
}
