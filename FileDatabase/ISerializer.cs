﻿using System.Collections.Generic;

namespace TaxiService.FileDatabase
{
    public interface ISerializer<T>
    {
        string Serialize(T obj);
        T Deserialize(string str);
        string GetExtension();
    }
}