﻿using FileDatabase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace TaxiService.FileDatabase
{
    public class SerializeJSON<T> : AbstractCommonForSerializer<T>
    {
        private static JavaScriptSerializer json = new JavaScriptSerializer();
        public override T Deserialize(string str)
        {
            try
            {
                return json.Deserialize<T>(str);
            }
            catch { return default(T); }
        }

        public override string Serialize(T obj)
        {
            return json.Serialize(obj).Trim('\0', '\x00');
        }

        public override string GetExtension() => "json";
    }
}
