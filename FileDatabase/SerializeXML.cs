﻿using FileDatabase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TaxiService;

namespace TaxiService.FileDatabase
{
    public class SerializeXML<T>: AbstractCommonForSerializer<T>
    {
        private static XmlSerializer sr = new XmlSerializer(typeof(T));
        public override T Deserialize(string str)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(str)))
            {
                var rez = (T)sr.Deserialize(stream);
                return rez;
            }
        }

        public override string Serialize(T obj)
        {
            using (var stream = new MemoryStream())
            {
                sr.Serialize(stream, obj);
                return Encoding.UTF8.GetString(stream.GetBuffer()).Trim('\0', '\x00');
            }
        }


        public override string GetExtension() => "xml";
    }
}
