﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService;
using TaxiService.FileDatabase;

namespace FileDatabase
{
    public abstract class AbstractCommonForSerializer<T>: ISerializer<T>
    {
        public abstract string Serialize(T obj);
        public abstract T Deserialize(string str);
        public abstract string GetExtension();
    }
}
