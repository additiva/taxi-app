﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;

namespace TaxiService.Repository
{
    public interface IRepository<T> where T: BaseEntity
    {
        void Persist(T data);
        T FindById(int id);
        void Remove(T data);
        IEnumerable<T> GetEntities();
    }
}
