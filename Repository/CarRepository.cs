﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.Store;

namespace TaxiService.Repository
{
    public class CarRepository : GenericRepository<Car>
    {
        public CarRepository(IStore<Car> store) : base(store)
        {
        }
    }
}
