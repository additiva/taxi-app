﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.Store;

namespace TaxiService.Repository
{
    public abstract class GenericRepository<TEntity>: IRepository<TEntity> where TEntity: BaseEntity
    {
        protected IStore<TEntity> _store;
        public GenericRepository(IStore<TEntity> store)
        {
            _store = store;
        }

        public TEntity FindById(int id)
        {
            return _store.FindById(id);
        }

        public IEnumerable<R> Get<R>() where R : BaseEntity
        {
            return _store.GetAll().OfType<R>();
        }

        public IEnumerable<TEntity> GetEntities()
        {
            return _store.GetAll();
        }

        public void Persist(TEntity data)
        {
            _store.Update(data);
        }

        public void Remove(TEntity data)
        {
            _store.Remove(data);
        }
        public void Flush()
        {
            _store.Flush();
        }
    }
}
