﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Store;

namespace TaxiService.Repository
{
    public class ReviewRepository : GenericRepository<Review>
    {
        public ReviewRepository(IStore<Review> store) : base(store)
        {
        }

        public IEnumerable<Review> GetTopReviews(int minStars = 1)
        {
            var data = GetEntities();
            var o = from p in data
                    where minStars <= p.Stars
                    select p;
            return o;
        }
        public IEnumerable<Review> GetWherePassangerNameIs(Func<Passenger, bool> r)
        {
            var o = GetEntities().Where(internalEntity => r(internalEntity.Passenger));
            return o;
        }
    }
}
