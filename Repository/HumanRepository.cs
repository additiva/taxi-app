﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;
using TaxiService.Store;

namespace TaxiService.Repository
{
    public class HumanRepository : GenericRepository<Human>
    {
        public HumanRepository(IStore<Human> store) : base(store)
        {
        }
    }
}
