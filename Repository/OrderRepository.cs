﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Store;

namespace TaxiService.Repository
{
    public class OrderRepository: GenericRepository<Order>
    {
        public OrderRepository(IStore<Order> store) : base(store)
        {
        }

        public IEnumerable<Order> GetBestOrders(int limit = 5)
        {
            return GetEntities().OrderBy(order => order.Duration).Take(limit);
        }
    }
}
