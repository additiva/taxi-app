﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService.Event
{
    public class OrderCompletedArgs: EventArgs
    {
        public readonly Order Order;
        public readonly Passenger Passenger;
        public readonly Operator Operator;
        public OrderCompletedArgs(Order or, Passenger p, Operator op)
        {
            Passenger = p;
            Operator = op;
            Order = or;
        }
    }
}
