﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Event;

namespace TaxiService.Manager
{
    public class OrderManager
    {
        public EventHandler<OrderCompletedArgs> orderCompleted;
        public void OnOrderCompleted(OrderCompletedArgs args)
        {
            orderCompleted?.Invoke(this, args);
        }
    }
}
