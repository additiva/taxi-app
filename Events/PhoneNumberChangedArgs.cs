﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService.Event
{
    using TaxiService;
    public class PhoneNumberChangedArgs: EventArgs
    {
        public readonly Human Person;
        public readonly string OldPhoneNumber;
        public readonly string NewPhoneNumber;
        public PhoneNumberChangedArgs(Human person, string newPhoneNumber)
        {
            Person = person;
            OldPhoneNumber = person.PhoneNumber;
            NewPhoneNumber = newPhoneNumber;
        }
    }
}
