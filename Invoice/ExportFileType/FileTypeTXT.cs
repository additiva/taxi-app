﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService.FileType
{
    public class FileTypeTXT : IFileType
    {
        public string GetExtension() => "txt";
        public byte[] GetBinaryData(StringBuilder o, Order r)
        {
            o.AppendFormat("[txt]: from {0}{1}", Encoding.UTF8.GetString(GetImageData(r.Origin)), Environment.NewLine);
            o.AppendFormat("[txt]: to {0}{1}", Encoding.UTF8.GetString(GetImageData(r.Destination)), Environment.NewLine);
            return Encoding.UTF8.GetBytes(o.ToString());
        }

        public byte[] GetImageData(GeoCoordinate gc)
        {
            return Encoding.UTF8.GetBytes(string.Format(StaticDependency.GetUrlForImage(), gc.ToString()));
        }
    }
}
