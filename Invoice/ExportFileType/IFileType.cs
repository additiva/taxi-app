﻿using System.Text;

namespace TaxiService
{
    public interface IFileType
    {
        byte[] GetBinaryData(StringBuilder o, Order r);
        string GetExtension();
        byte[] GetImageData(System.Device.Location.GeoCoordinate gc);
    }
}