﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.IO;
using System.Device.Location;

namespace TaxiService.FileType
{
    public class FileTypePDF : IFileType
    {
        public string GetExtension() => "pdf";
        public byte[] GetBinaryData(StringBuilder o, Order r)
        {
            PdfDocument document = new PdfDocument();
            document.Info.Title = "Invoice Created for taxi-app";
            PdfPage page = document.AddPage();

            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            gfx.DrawRoundedRectangle(new XPen(XColor.FromKnownColor(XKnownColor.Black), 3), new System.Drawing.Rectangle(10, 10, 492, 772), new System.Drawing.Size(2, 2));
            // Create a 
            XFont font = new XFont("Verdana", 12, XFontStyle.Regular);

            string s = o.ToString();
            int xx = 0;
            foreach (var x in s.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
            {
                xx += 16;
                // Draw the text
                gfx.DrawString(x, font, XBrushes.Black,
                  new XRect(20, xx, page.Width, page.Height),
                  XStringFormats.TopLeft);
            }
            try
            {
                gfx.DrawImage(XImage.FromGdiPlusImage(System.Drawing.Image.FromStream(new MemoryStream(GetImageData(r.Origin)))), 15, 270);
                gfx.DrawString("From", font, XBrushes.Blue, 15, 250);
                gfx.DrawString("To", font, XBrushes.Blue, 15, 520);
                gfx.DrawImage(XImage.FromGdiPlusImage(System.Drawing.Image.FromStream(new MemoryStream(GetImageData(r.Destination)))), 15, 540);
            }
            catch (ArgumentException e)
            {
                gfx.DrawString("Failed to draw string, exception: " +
                    e.GetType().ToString()
                    , font, XBrushes.Blue, 15, 250);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                document.Save(ms);
                return ms.GetBuffer();
            }
        }

        public byte[] GetImageData(GeoCoordinate gc)
        {
            string url = string.Format(StaticDependency.GetUrlForImage(), gc.ToString());
            try
            {
                return Task.Run(() => StaticDependency.GetHttpClient().GetByteArrayAsync(url)).Result;
            }
            catch (AggregateException)
            {
                return new byte[] { };
            }
        }
    }
}
