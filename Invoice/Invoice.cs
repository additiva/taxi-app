﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using TaxiService;

namespace TaxiService
{
    public static class Invoice<T> where T : IFileType, new()
    {
        private static T formatter = new T();
        public static byte[] GetFullInvoice(Order o)
        {
            return formatter.GetBinaryData(PrependToInvoice(o), o);
        }
        private static StringBuilder PrependToInvoice(Order o)
        {
            var r = new StringBuilder();
            r.AppendFormat(
                @"Distance: {0}km{1}", o.Distance, Environment.NewLine
            );
            r.AppendFormat(
                @"Departure Time: {0}{1}", o.DepartureTime.ToString(StaticDependency.GetDateTimeFormat()), Environment.NewLine
            );
            r.AppendFormat(
                @"Arrived Time: {0}{1}", o.ArrivalTime.ToString(StaticDependency.GetDateTimeFormat()), Environment.NewLine
            );
            r.AppendFormat(
                @"Time spended on way: {0}{1}", o.Duration, Environment.NewLine
            );
            r.Append('-', 20);
            r.Append(Environment.NewLine);

            r.AppendFormat(
                @"From coord: {0}{1}", o.Origin, Environment.NewLine
            );
            r.AppendFormat(
                @"To coord: {0}{1}", o.Destination, Environment.NewLine
            );
            r.Append('-', 20);
            r.Append(Environment.NewLine);

            r.AppendFormat(
                @"Driver: {0}{1}", o.Car?.Driver, Environment.NewLine
            );
            r.AppendFormat(
                @"Operator: {0}{1}", o.Operator, Environment.NewLine
            );
            r.Append('-', 20);
            r.Append(Environment.NewLine);
            r.AppendFormat(
                @"Total Price: {0:0.00}{1}", o.GetFinalPrice(), Environment.NewLine
            );
            return r;
        }
        public static void SaveAndOpen(Order o, string fileName)
        {
            fileName = Save(o, fileName);

            Process.Start(fileName);
        }
        public static string Save(Order o, string fileName)
        {
            fileName = Path.GetFileNameWithoutExtension(fileName);
            fileName = string.Format("{0}.{1:3}", fileName, formatter.GetExtension());
            byte[] byteArray = GetFullInvoice(o);
            FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            fs.Write(byteArray, 0, byteArray.Length);
            fs.Close();
            return Path.GetFullPath(fileName);
        }
    }
}