﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService;
using TaxiService.Entity;
using TaxiService.FileDatabase;
using TaxiService.Store;

namespace Infrastructure
{
    public static class InjectedServiceLocator
    {
        private static readonly IKernel Kernel = new StandardKernel();

        public static void RegisterAll()
        {
            Kernel.Bind(typeof(ISerializer<>)).To(typeof(SerializeXML<>));

            Kernel.Bind(typeof(IEqualityComparer<>)).To(typeof(TaxiService.Entity.BaseEntityEqualityComparer));
            Kernel.Bind(typeof(MemoryStore<Human>)).ToSelf();

            Kernel.Bind(typeof(IStore<Car>)).To(typeof(FileStore<Car>)).InSingletonScope().Named("CarStore").WithConstructorArgument("fileName", "cars");
            Kernel.Bind(typeof(IStore<Human>)).To(typeof(FileStore<Human>)).InSingletonScope().Named("HumanStore").WithConstructorArgument("fileName", "humans");
            Kernel.Bind(typeof(IStore<Order>)).To(typeof(FileStore<Order>)).InSingletonScope().Named("OrderStore").WithConstructorArgument("fileName", "orders");
            Kernel.Bind(typeof(IStore<Review>)).To(typeof(FileStore<Review>)).InSingletonScope().Named("ReviewStore").WithConstructorArgument("fileName", "reviews");
        }

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }

    }
}