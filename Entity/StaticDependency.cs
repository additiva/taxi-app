﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService
{
    public static class StaticDependency
    {
        public static class DbFileNames
        {
            public static string Order = "orders";
            public static string Review = "reviews";
            public static string Car = "cars";
            public static string Human = "humans";
        }
        private static HttpClient _client;
        private static DateTimeFormatInfo _dateformat;
        public static string BinaryDelimiterForSerialization = "|\x30\x40\x50\x60\x10";
        private static object _locked_hm = new object();
        private static object _locked_om = new object();
        private static object _locked_client = new object();
        private static object _locked_dateformat = new object();
        public static DateTimeFormatInfo GetDateTimeFormat()
        {
            if (_dateformat == null)
            {
                lock (_locked_dateformat)
                {
                    _dateformat = _dateformat ?? CultureInfo.GetCultureInfo("ro-MD").DateTimeFormat;
                }
            }
            return _dateformat;
        }
        public static HttpClient GetHttpClient()
        {
            if (_client == null)
            {
                lock (_locked_client)
                {
                    _client = _client ?? new HttpClient();
                }
            }
            return _client;
        }
        public static string GetUrlForImage() => "https://maps.googleapis.com/maps/api/staticmap?center={0}&zoom=10&size=400x300";
        public static string NormalizeFileName(string fileName, string expectedExtension)
        {
            fileName = Path.GetFileNameWithoutExtension(fileName);
            fileName = string.Format("{0}.{1}", fileName, expectedExtension);
            return fileName;
        }
    }
}
