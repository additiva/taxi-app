﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxiService;

namespace TaxiService.Event
{
    public static class EventIntializer
    {
        public static void InitDelegates()
        {
            Order.OrderManager.orderCompleted += new EventHandler<OrderCompletedArgs>((q, b) =>
                MessageBox.Show("Arrived: \n" + b.Order.ToString(), "arrived", MessageBoxButtons.OK, MessageBoxIcon.Information));
            Human.HumanManager.phoneNumberChanged += new EventHandler<PhoneNumberChangedArgs>((a, b) =>
                MessageBox.Show("Old number: " + b.OldPhoneNumber + "\nnewNumber: " + b.NewPhoneNumber, "change number", MessageBoxButtons.OK, MessageBoxIcon.Information));

        }
    }
}
