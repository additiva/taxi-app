﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService.Manager
{
    using TaxiService.Event;
    public class HumanManager
    {
        public EventHandler<PhoneNumberChangedArgs> phoneNumberChanged;
        public void OnPhoneNumberChanged(PhoneNumberChangedArgs args)
        {
            phoneNumberChanged?.Invoke(this, args);
        }
    }
}
