﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService.Entity
{
    public abstract class BaseEntity: IComparable<BaseEntity>
    {
        public int Id
        {
            get
            {
                return this.GetHashCode();
            }
            set
            {

            }
        }
        public override int GetHashCode()
        {
            return Math.Abs((int)CH.Crc32.Crc.Crc32(Encoding.UTF8.GetBytes(ToString())));
        }
        public override bool Equals(object obj)
        {
            return GetHashCode() == ((BaseEntity)obj).GetHashCode();
        }

        public int CompareTo(BaseEntity other)
        {
            return this.GetHashCode() - other.GetHashCode();
        }
    }
}
