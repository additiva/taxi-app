﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiService
{
    public class Driver : TaxiService.Human
    {
        public Driver():base()
        {

        }
        public Driver(string fullName, DateTime bday, string phoneNumber, DateTime registeredDate, bool isMale = true) : base(fullName, bday, phoneNumber, isMale)
        {
            RegisteredDate = registeredDate;
        }

        public DateTime RegisteredDate { get; set; }

        public double? SallaryBonus { get; set; }

        public override void RegisterOrder(Order order)
        {
            SallaryBonus = SallaryBonus ?? 0;
            SallaryBonus += 1e-2;
        }
    }
}