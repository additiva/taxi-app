﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxiService.Manager;

namespace TaxiService
{
    public class Passenger : TaxiService.Human
    {
        public Passenger(): base()
        {

        }
        public int NumberOfOrders { get; set; } = 0;
        public Passenger(string fullName, DateTime bday, string phoneNumber, bool isMale = true) : base(fullName, bday, phoneNumber, isMale)
        {
        }
        public override void RegisterOrder(Order order)
        {
            NumberOfOrders++;
        }
    }
}