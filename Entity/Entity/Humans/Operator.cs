﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxiService.Manager;

namespace TaxiService
{
    public class Operator : TaxiService.Human
    {
        public Operator():base()
        {

        }
        public int OrdersProcessed { get; set; }

        public Operator(string fullName, DateTime bday, string phoneNumber, bool isMale = true) : base(fullName, bday, phoneNumber, isMale)
        {
        }
        public override void RegisterOrder(Order order)
        {
            OrdersProcessed++;
        }
    }
}