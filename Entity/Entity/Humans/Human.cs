﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TaxiService;
using TaxiService.Entity;
using TaxiService.Manager;

namespace TaxiService
{
    [XmlInclude(typeof(Passenger))]
    [XmlInclude(typeof(Operator))]
    [XmlInclude(typeof(Driver))]
    public abstract class Human: BaseEntity, IComparable
    {
        protected Human()
        {

        }
        public readonly static HumanManager HumanManager = new HumanManager();
        protected Human(string fullName, DateTime bday, string phoneNumber, bool isMale = true)
        {
            FullName = fullName;
            BirthDate = bday;
            PhoneNumber = phoneNumber;
            IsMale = isMale;
        }
        public bool IsMale { get; set; } = true;
        public string FullName { get; set; }
        public abstract void RegisterOrder(Order order);

        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                if (value != _phoneNumber)
                {
                    if (!string.IsNullOrWhiteSpace(_phoneNumber))
                        HumanManager.OnPhoneNumberChanged(new Event.PhoneNumberChangedArgs(this, value));
                    _phoneNumber = value;
                }
            }
        }

        public readonly DateTime BirthDate;
        private string _phoneNumber;

        public int CompareTo(object obj)
        {
            Human compareTo = obj as Human;
            if (compareTo == null)
                return -1;
            return this.FullName.CompareTo(compareTo.FullName);
        }
        public override string ToString()
        {
            return string.Format("Name = {0}, PhoneNumber = {1}", FullName, PhoneNumber);
        }
    }
}