﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiService.Entity;

namespace TaxiService.Entity
{
    public class BaseEntityEqualityComparer : IEqualityComparer<BaseEntity>
    {
        public bool Equals(BaseEntity x, BaseEntity y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(BaseEntity obj)
        {
            return obj.GetHashCode();
        }
    }
}
