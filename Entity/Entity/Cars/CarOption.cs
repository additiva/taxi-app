﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiService
{
    [Flags]
    public enum CarOptions { None = 1, Music = 2, SunRoof = 4, Wifi = 8, ChildSeat = 16 }
}
