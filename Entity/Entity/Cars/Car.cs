﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TaxiService.Entity;

namespace TaxiService
{
    [XmlInclude(typeof(SedanCar))]
    [XmlInclude(typeof(TruckCar))]
    public abstract class Car: BaseEntity
    {
        public CarOptions Options { get; set; }

        public string VIN { get; set; }

        public Driver Driver { get; set; }
        public CarType CarType { get; set; }

        public System.Device.Location.GeoCoordinate CurrentLocation { get; set; }
        protected Car()
        {

        }
        public override string ToString()
        {
            return string.Format("Car Vin - {0}, Car Type - {1}", VIN, CarType);
        }

    }
}