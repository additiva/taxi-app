﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaxiService
{
    [Serializable]
    public class TruckCar : Car
    {
        public TruckCar()
        {

        }
        public double TrunkVolume { get; set; }
        public TruckCar(double trunkVolume)
        {
            TrunkVolume = trunkVolume;
        }
    }
}