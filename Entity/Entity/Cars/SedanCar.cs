﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace TaxiService
{
    [Serializable]
    public class SedanCar : Car
    {
        public SedanCar()
        {

        }
        public int NumberOfSeats { get; set; }
        public SedanCar(int numberOfSeats)
        {
            NumberOfSeats = numberOfSeats;
        }
        public SedanCar(int numberOfSeats, string vin)
        {
            NumberOfSeats = numberOfSeats;
            VIN = vin;
        }
    }
}