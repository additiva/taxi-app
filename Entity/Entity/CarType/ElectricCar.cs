﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxiService.Entity;

namespace TaxiService
{
    public class ElectricCar : CarType
    {
        public ElectricCar(): base()
        {

        }
        public ElectricCar(double killowattPerKm, double pricePerKillowatt)
        {
            KilowattPerKm = killowattPerKm;
            PricePerKillowatt = pricePerKillowatt;
        }
        public double KilowattPerKm { get; set; }
        public double PricePerKillowatt { get; set; }

        public override double GetPriceForKm(Order order)
        {
            var pricePerKm = ClearPriceForOneKillometer();
            if (order.IsUrbanStyle)
                pricePerKm *= 1.5;
            return pricePerKm;
        }

        private double ClearPriceForOneKillometer()
        {
            return PricePerKillowatt * PricePerKillowatt;
        }
        public override string ToString()
        {
            return string.Format($"killowatt/km = {KilowattPerKm}, priceperkillowatt = {PricePerKillowatt}");
        }
    }
}