﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxiService.Entity;

namespace TaxiService
{
    public class GasolineCar : CarType
    {
        public GasolineCar(): base()
        {

        }
        public double LitresPerKm { get; set; }
        public double PricePerLitre { get; set; }
        public GasolineCar(double litresPerKm, double pricePerLitre)
        {
            LitresPerKm = litresPerKm;
            PricePerLitre = pricePerLitre;
        }

        public override double GetPriceForKm(Order order)
        {
            return LitresPerKm * PricePerLitre;
        }
        public override string ToString()
        {
            return string.Format($"litres/km = {LitresPerKm}, priceperlitre = {PricePerLitre}");
        }
    }
}