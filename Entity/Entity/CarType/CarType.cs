﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TaxiService.Entity;

namespace TaxiService
{
    [XmlInclude(typeof(ElectricCar))]
    [XmlInclude(typeof(GasolineCar))]
    public abstract class CarType: BaseEntity
    {
        protected CarType()
        {

        }
        public abstract double GetPriceForKm(Order order);
    }
}