﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using TaxiService.Entity;
using TaxiService.Manager;

namespace TaxiService
{
    public class Order: BaseEntity
    {
        protected Order()
        {

        }
        public readonly static OrderManager OrderManager = new OrderManager();
        public Order(Passenger passenger, DateTime departureTime, GeoCoordinate origin, GeoCoordinate dest, Car car, bool urbanStyle = true)
        {
            if (null == passenger)
                throw new ArgumentNullException(nameof(passenger));
            if (null == origin)
                throw new ArgumentNullException(nameof(origin));
            if (null == dest)
                throw new ArgumentNullException(nameof(dest));
            if (null == car)
                throw new ArgumentNullException(nameof(car));

            IsUrbanStyle = urbanStyle;
            Passenger = passenger;
            DepartureTime = departureTime;
            Origin = origin;
            Destination = dest;
            Car = car;
        }
        public void Arrived(DateTime current)
        {
            ArrivalTime = current;
            OrderManager.OnOrderCompleted(new Event.OrderCompletedArgs(this, Passenger, Operator));
        }
        public void Arrived()
        {
            Arrived(DateTime.Now);
            FullyProcessed = true;
            Passenger?.RegisterOrder(this);
            Operator?.RegisterOrder(this);
            Car?.Driver?.RegisterOrder(this);
        }
        public bool FullyProcessed { get; set; }
        public GeoCoordinate Origin { get; set; }
        public GeoCoordinate Destination { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public TimeSpan Duration => ArrivalTime - DepartureTime;
        public Car Car { get; set; }
        public Operator Operator { get; set; }
        public Passenger Passenger { get; set; }
        public Review Review { get; set; }
        public double Distance => Origin.GetDistanceTo(Destination) / 1000;
        public bool IsUrbanStyle { get; set; }

        public double GetFinalPrice()
        {
            double pricePerOneKm = Car.CarType?.GetPriceForKm(this) ?? 1;
            return pricePerOneKm * Distance;
        }
        public override string ToString()
        {
            return string.Format("Operator: {0},\n Passanger: {1},\n DepartureTime: {2},\n ArrivalTime: {3}", 
                Operator, 
                Passenger, 
                DepartureTime.ToString(StaticDependency.GetDateTimeFormat()), ArrivalTime.ToString(StaticDependency.GetDateTimeFormat()));
        }
    }
}