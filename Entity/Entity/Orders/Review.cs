﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using TaxiService.Entity;

namespace TaxiService
{
    [Serializable]    
    public class Review: BaseEntity
    {
        public Review() { }
        public Review(Order order, Passenger passenger, string title, string descr, int stars)
        {
            if (null == order)
                throw new ArgumentNullException("order");
            if (null == passenger)
                throw new ArgumentNullException("passenger");
            if (order.Passenger != passenger)
                throw new ArgumentException("Passanger from order need to write review for this particular order");
            Passenger = passenger;
            Order = order;
            Title = title;
            Description = descr;
            Stars = stars;
        }
        public Passenger Passenger { get; set; }
        public Order Order { get; set; }

        public string Description { get; set; }

        public string Title { get; set; }

        public int Stars { get; set; }
    }
}